#include <CommandInterpreter.h>

CommandInterpreter CLI(Serial);
Command del13("led", del13Command, "Change l'etat de la DEL 13");

void del13Command(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  switch (argCount) {
    case 0:
      // affiche l'état actuel
      if (digitalRead(13) == HIGH) serial.println(F("on"));
      else serial.println(F("off"));
      break;
    case 1:
      // change l'état
      char *arg;
      if (cmdInt.readString(arg)) {
        if (strcmp(arg, "on") == 0) digitalWrite(13, HIGH);
        else if (strcmp(arg, "off") == 0)  digitalWrite(13, LOW);
        else serial.println(F("Argument incorrect"));
      }
      else serial.println(F("Argument incorrect"));
      break;
    default:
      serial.println(F("Nombre d'arguments incorrect"));
      break;
  }
}

void setup()
{
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  CLI.addCommand(del13);
  CLI.setPrompt(F("led>"));
}

void loop()
{
  CommandInterpreter::update();
}

