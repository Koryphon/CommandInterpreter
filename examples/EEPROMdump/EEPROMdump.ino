#include <EEPROM.h>
#include <CommandInterpreter.h>

CommandInterpreter CLI(Serial);
Command dumpEEPROM("rep", EEPROMdump, "Affiche le contenu de l'EEPROM");
Command writeEEPROM("wep", EEPROMwrite, "Ecrit dans l'EEPROM");
Command sizeEEPROM("size", EEPROMsize, "Affiche la taille de l'EEPROM");

void printHexAddress(HardwareSerial &serial, unsigned int address)
{
  if (address < 16) serial.print('0');
  if (address < 256) serial.print('0');
  serial.print(address, HEX);
  serial.print(": ");
}

void EEPROMdump(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned int startAddress;
  unsigned int length;
  bool printHelp = false;
  
  serial.println(argCount);
  
  switch (argCount) {

    case 1:
      if (cmdInt.readUnsignedInt(startAddress)) {
        printHexAddress(serial, startAddress);
        byte value = EEPROM.read(startAddress);
        if (value < 16) serial.print('0');
        serial.println(value, HEX);
      }
      else printHelp = true;
      break;
      
    case 2:
      if (cmdInt.readUnsignedInt(startAddress) && cmdInt.readUnsignedInt(length)) {
        unsigned int address = startAddress;
        unsigned int counter = 0;
        printHexAddress(serial, address);
        while (counter < length && address <= E2END) {
          byte value = EEPROM.read(address++);
          if (value < 16) serial.print('0');
          serial.print(value, HEX);
          serial.print(' ');
          if (++counter % 16 == 0 && counter < length) {
            serial.println();
            printHexAddress(serial, address);
          }
        } 
        serial.println();
      }
      else printHelp = true;
      break;
    
    default:
      serial.println(F("Nombre d'arguments incorrect"));
      printHelp = true;
      break;
  }
  if (printHelp) serial.println(F("Usage : rep adresseDebut <longueur> ou rep addresse"));
}

void EEPROMwrite(CommandInterpreter &cmdInt, byte argCount)
{
  unsigned int address;
  byte value;
  byte compteur = 0;
  bool printHelp = false;
  
  if (argCount >= 2) {
    if (cmdInt.readUnsignedInt(address)) {
      if (address > E2END) cmdInt.Serial().print(F("Adresse au dela de la taille"));
      else {
        while(cmdInt.readByte(value) && address <= E2END) {
          compteur++;
          EEPROM.write(address++, value);
        }
        cmdInt.Serial().print(compteur);
        cmdInt.Serial().println(F(" octets ecrits"));
      }
    }
    else printHelp = true;
  } else printHelp = true;
  
  if (printHelp) cmdInt.Serial().println(F("Usage : wep adresse valeur <valeur>"));
}

void EEPROMsize(CommandInterpreter &cmdInt, byte argCount)
{
  cmdInt.Serial().println(E2END + 1);
  if (argCount > 0) cmdInt.Serial().println(F("Argument(s) ingnore(s)"));
}

void setup() {
  Serial.begin(9600);
  CLI.setPrompt(F("EEPROM>"));
  CLI.addCommand(dumpEEPROM);
  CLI.addCommand(writeEEPROM);
  CLI.addCommand(sizeEEPROM);
}

void loop() {
  CommandInterpreter::update();
}

