/*
 * 
 */

#include "CommandInterpreter.h"

enum { COMMAND_WAITING, NEW_COMMAND };

static const byte DELETE_CODE = 0x08;
static const byte UP_CODE     = 0x1E;
static const byte DOWN_CODE   = 0x1F;
static const byte BELL_CODE   = 0x07;

Command::Command(const char *commandString, commandFunction function, const char *helpString)
  : mCommandString(commandString),
    mFunction(function),
    mHelpString(helpString),
    mInterpreter(NULL)
{
}

bool Command::operator==(const char *commandString)
{
  return strcmp(mCommandString, commandString) == 0;
}

void Command::execute(byte argCount)
{
  mFunction(*mInterpreter, argCount);
}

void Command::help(byte maxLen)
{
  HardwareSerial &serial = mInterpreter->Serial();
  byte commandLen = mInterpreter->Serial().print(mCommandString);
  serial.write(':');
  for (byte i = 0; i < maxLen + 1 - commandLen; i++) serial.write(' ');
  serial.println(mHelpString);
}

void CommandInterpreterBuffer::printHistory(HardwareSerial &serial)
{
  byte index = (mHistoryBufferWriteIndex - mHistorySize) & 0x7;
  byte count = mHistorySize;
  while (count-- > 0) {
    serial.println(mHistoryBuffer[index]);
    index = (index + 1) & 0x7;
  }
}

void CommandInterpreterBuffer::pushInHistory() {

  byte index = (mHistoryBufferWriteIndex - 1) & 0x7;
  if (! strncmp(mHistoryBuffer[index], mWorkingBuffer, COMMAND_BUFFER_SIZE) == 0) {
    strncpy(mHistoryBuffer[mHistoryBufferWriteIndex], mWorkingBuffer, COMMAND_BUFFER_SIZE);
    mHistoryBufferWriteIndex = (mHistoryBufferWriteIndex + 1) & 0x07;
    if (mHistorySize < HISTORY_SIZE) mHistorySize++;
  }
  mHistoryBufferSelected = 0;
  mNavigateThroughHistory = false;
  mWorkingBufferIndex = 0;
  mWorkingBuffer[0] = '\0';
}

void CommandInterpreterBuffer::reset()
{
  mWorkingBufferIndex = 0;
  mWorkingBuffer[0] = '\0';
}  


void CommandInterpreterBuffer::upInHistory(HardwareSerial& serial) {
  if (mHistorySize > 0) {
    if (! mNavigateThroughHistory) {
      mNavigateThroughHistory = true;
      strncpy(mWorkingBufferSave, mWorkingBuffer, COMMAND_BUFFER_SIZE);
      mHistoryBufferSelected = 0;
    }
    if (mHistoryBufferSelected < mHistorySize) {
      mHistoryBufferSelected++;
      strncpy(mWorkingBuffer,
              &mHistoryBuffer[(mHistoryBufferWriteIndex - mHistoryBufferSelected) & 0x07][0],
              COMMAND_BUFFER_SIZE);
      /* clean the command line */
      for (byte i = 0; i < mWorkingBufferIndex; i++) serial.write(DELETE_CODE);
      /* update the command line with the current working buffer */
      serial.print(mWorkingBuffer);
      mWorkingBufferIndex = strlen(mWorkingBuffer);
    }
  }
}

void CommandInterpreterBuffer::downInHistory(HardwareSerial& serial) {
  if (mNavigateThroughHistory) {
    if (mHistoryBufferSelected > 1) {
      mHistoryBufferSelected--;
      strncpy(mWorkingBuffer,
              &mHistoryBuffer[(mHistoryBufferWriteIndex - mHistoryBufferSelected) & 0x07][0],
              COMMAND_BUFFER_SIZE);
    }
    else {
      /* reached the buffer */
      mNavigateThroughHistory = false;
      strncpy(mWorkingBuffer, mWorkingBufferSave, COMMAND_BUFFER_SIZE);
    }
    /* clean the command line */
    for (byte i = 0; i < mWorkingBufferIndex; i++) serial.write(DELETE_CODE);
    /* update the command line with the current working buffer */
    serial.print(mWorkingBuffer);
    mWorkingBufferIndex = strlen(mWorkingBuffer);
  }
}


char *CommandInterpreterBuffer::getCommandAndArgCount(byte &argCount)
{
  byte count = 0;
  strncpy(mWorkingBufferSave, mWorkingBuffer, COMMAND_BUFFER_SIZE);
  char *tok = strtok(mWorkingBufferSave, " ");
  while (tok = strtok(NULL, " ")) count++;
  argCount = count;
  strncpy(mWorkingBufferSave, mWorkingBuffer, COMMAND_BUFFER_SIZE);
  return strtok(mWorkingBufferSave, " ");;
}

CommandInterpreter *CommandInterpreter::sInterpreterList = NULL;

CommandInterpreter::CommandInterpreter(HardwareSerial &serialLink)
  : mSerialLink(serialLink),
    mPrompt((__FlashStringHelper *)NULL),
    mState(NEW_COMMAND),
    mTerminator('\n'),
    mCommandList(NULL),
    mCommandStringLength(4) // len of help string
{
  mNextInterpreter = sInterpreterList;
  sInterpreterList = this;
}

void CommandInterpreter::addCommand(Command &command)
{
  command.mInterpreter = this;
  command.mNextCommand = mCommandList;
  mCommandList = &command;
  mCommandStringLength = max(mCommandStringLength,
                             strlen(command.mCommandString));
}

void CommandInterpreter::printPrompt()
{
  if (mPrompt != (__FlashStringHelper *)NULL) {
    PGM_P p = reinterpret_cast<PGM_P>(mPrompt);
    while (1) {
      unsigned char c = pgm_read_byte(p++);
      if (c == 0) break;
      mSerialLink.write(c);
    }
  }
  else mSerialLink.write('>');
  mSerialLink.write(' ');
}

void CommandInterpreter::updateInterpreter()
{
  char c;

  if (mState == NEW_COMMAND) {
    printPrompt();
    mState = COMMAND_WAITING;
  }

  bool getCharacters = true;
  while (mSerialLink.available() > 0 &&
         getCharacters)
  {
    c = mSerialLink.read();
    
    switch (c) {
      case DELETE_CODE:
        if (mBuffer.deleteLastChar()) mSerialLink.write(DELETE_CODE);
        break;
      case UP_CODE:
        mBuffer.upInHistory(mSerialLink); 
        break;
      case DOWN_CODE:
        mBuffer.downInHistory(mSerialLink); 
        break;
      default:
        if (c == mTerminator) {
          getCharacters = false;
          mSerialLink.write(c);
        }
        else if (c >= 0x20 && c <= 0x7D && mBuffer.space() > 0) {
          mSerialLink.write(c);
          mBuffer.addChar(c);
        }
        else mSerialLink.write(BELL_CODE);
        break;
    }
  }
  
  if (c == mTerminator) { // got a command, process it
    // Get the command
    byte argCount;
    char *commandString = mBuffer.getCommandAndArgCount(argCount);
    
    bool commandOk = false;
    if (strcmp(commandString, "help") == 0) {
      help();
      commandOk = true;
    }
    else if (strcmp(commandString, "hist") == 0) mBuffer.printHistory(mSerialLink);
    else {
      Command *cmd = mCommandList;
      while (cmd) {
        if (*cmd == commandString) break;
        cmd = cmd->mNextCommand;
      }
      if (cmd) {
        cmd->execute(argCount);
        commandOk = true;
      }
      else if (! mBuffer.isEmpty()) mSerialLink.println(F("Command not found"));
    }
   
    if (commandOk) mBuffer.pushInHistory();
    else mBuffer.reset();

    mState = NEW_COMMAND;
  }    
}

void CommandInterpreter::help()
{
  mSerialLink.print(F("help:"));
  for (byte i = 0; i < mCommandStringLength + 1 - 4; i++)
    mSerialLink.write(' ');
  mSerialLink.println(F("affiche cette liste"));
  
  mSerialLink.print(F("hist:"));
  for (byte i = 0; i < mCommandStringLength + 1 - 4; i++)
    mSerialLink.write(' ');
  mSerialLink.println(F("affiche l'historique"));
  
  Command *currentCommand = mCommandList;
  while (currentCommand) {
    currentCommand->help(mCommandStringLength);
    currentCommand = currentCommand->mNextCommand;
  }
}

void CommandInterpreter::update()
{
  CommandInterpreter *interpreter = sInterpreterList;
  while (interpreter) {
    interpreter->updateInterpreter();
    interpreter = interpreter->mNextInterpreter;
  }
}

bool CommandInterpreter::readString(char * &result) 
{
  result = mBuffer.getArg();
  return result != NULL;
}

bool CommandInterpreter::isANumber(char *string)
{
  if (string == NULL) return false;
  if (*string++ == '0') {
    if (*string == '\0') return true;
    if (*string == 'x' || *string == 'X') {
      // hexadecimal number
      string++;
	    while ((*string >= '0' && *string <= '9') ||
	           (*string >= 'A' && *string <= 'F') ||
	           (*string >= 'a' && *string <= 'f')) string++;
	  }
	  else {
	    // octal number
	    while (*string >= '0' && *string <= '7') string++;
	  }
	}
	else {
	  // decimal number
		while (*string >= '0' && *string <= '9') string++;
	}
	return *string == '\0';	  
}

bool CommandInterpreter::readByte(byte& result) 
{
  char **endChar;
  char *arg = mBuffer.getArg();
  if (! isANumber(arg)) return false;
  long longResult = strtol(arg, endChar, 0);
  if (**endChar != '\0' || longResult < 0 || longResult > 255) return false;
  else {
    result = byte(longResult);
    return true;
  }
}

bool CommandInterpreter::readInt(int& result) 
{
  char **endChar;
  char *arg = mBuffer.getArg();
  if (! isANumber(arg)) return false;
  long longResult = strtol(arg, endChar, 0);
  if (**endChar != '\0' || longResult < -32768 || longResult > 32767) return false;
  else {
    result = int(longResult);
    return true;
  }
}

bool CommandInterpreter::readUnsignedInt(unsigned int& result) 
{
  char **endChar;
  char *arg = mBuffer.getArg();
  if (! isANumber(arg)) return false;
  long longResult = strtol(arg, endChar, 0);
  if (**endChar != '\0' || longResult < 0 || longResult > 65535) return false;
  else {
    result = (unsigned int)longResult;
    return true;
  }
}

