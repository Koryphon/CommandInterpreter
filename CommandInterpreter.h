/*
 * 
 */

#include "Arduino.h"
#include "HardwareSerial.h"

static const byte COMMAND_BUFFER_SIZE = 20;
static const byte HISTORY_SIZE = 8;

class Command;
class CommandInterpreter;

/*
 * Prototype of a command function. The first arg is a reference to
 * the command in order to get the arguments and the second one
 * is the number of arguments.
 */
typedef void (*commandFunction)(CommandInterpreter&, byte);

class Command
{
    friend class CommandInterpreter;
    
  private:
    const char *mCommandString;
    const char *mHelpString;
    commandFunction mFunction;
    Command *mNextCommand;
    CommandInterpreter *mInterpreter;
 
  public:
    Command(const char *commandString, commandFunction function, const char *helpString);
    bool operator==(const char *commandString);
    void execute(byte argCount);
    void help(byte maxLen);
};


class CommandInterpreterBuffer
{
  private:
    char mWorkingBuffer[COMMAND_BUFFER_SIZE];
    char mWorkingBufferSave[COMMAND_BUFFER_SIZE];
    char mHistoryBuffer[HISTORY_SIZE][COMMAND_BUFFER_SIZE];
    byte mHistorySize;
    byte mWorkingBufferIndex;
    byte mHistoryBufferWriteIndex;
    byte mHistoryBufferSelected;
    bool mNavigateThroughHistory;
    
  public:
    CommandInterpreterBuffer() :
      mHistorySize(0),
      mWorkingBufferIndex(0),
      mHistoryBufferWriteIndex(0),
      mHistoryBufferSelected(0),
      mNavigateThroughHistory(false)
    {
      mWorkingBuffer[0] = '\0';
    }
    
    bool addChar(char c) {
      if (mWorkingBufferIndex == COMMAND_BUFFER_SIZE - 1) return false;
      else {
        mWorkingBuffer[mWorkingBufferIndex++] = c;
        mWorkingBuffer[mWorkingBufferIndex] = '\0';
        return true;
      }
    }
    
    bool deleteLastChar() {
      bool result = mWorkingBufferIndex > 0;
      if (result) mWorkingBuffer[--mWorkingBufferIndex] = '\0';
      return result;
    }
    
    byte space()   { return COMMAND_BUFFER_SIZE - 1 - mWorkingBufferIndex; }
    bool isEmpty() { return mWorkingBufferIndex == 0; }
    
    char *getCommandAndArgCount(byte &argCount);
    
    char *getArg() {
      return strtok(NULL, " ");
    }
    
    void print(HardwareSerial &serial) { serial.println(mWorkingBuffer); }
    
    void pushInHistory();
    void reset();
    void printHistory(HardwareSerial& serial);    
    void upInHistory(HardwareSerial& serial);
    void downInHistory(HardwareSerial& serial);
};

class CommandInterpreter
{
  private:
    CommandInterpreter        *mNextInterpreter;    // linked list of interpreter
    __FlashStringHelper       *mPrompt;             // prompt string
    byte                      mState;               // state of the interpreter
    byte                      mTerminator;          // end of line character
    byte                      mCommandStringLength; // max len of command
    HardwareSerial            &mSerialLink;         // the serial it uses
    Command                   *mCommandList;        // the list of commands
    CommandInterpreterBuffer  mBuffer;              // buffer, including history

    static CommandInterpreter *sInterpreterList;
    void updateInterpreter();
    void printPrompt();
    bool isANumber(char *string);
    
  public:
    CommandInterpreter(HardwareSerial &serialLink);
    void setPrompt(const __FlashStringHelper *prompt) { mPrompt = (__FlashStringHelper *)prompt; }
    void addCommand(Command &command);
    void help();
    void setTerminator(byte terminator) { mTerminator = terminator; }
    HardwareSerial &Serial() { return mSerialLink; }
    bool readString(char * &result);
    bool readByte(byte& result);
    bool readInt(int& result);
    bool readUnsignedInt(unsigned int& result);
    static void update();
};

